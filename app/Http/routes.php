<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('confirm', function () {
    return view('confirm');
});

Route::auth();

Route::get('/home', 'GuestController@index');
Route::post('guest/submit' ,'SubmitController@guest');
Route::resource('guests' ,'GuestController');
