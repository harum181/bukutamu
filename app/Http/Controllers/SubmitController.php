<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Guest;

class SubmitController extends Controller
{
    public function return()
    {
    	return view('confirm');
    }

    public function guest(Request $request)
    {   
        $this->validate($request, [
            'name' => 'required',
            'year' => 'required',
            'telp' => 'required',
            // 'instagram' => 'required',
            // 'linkedin' => 'required',
            'message' => 'required',
        ]);
        
        $data = $request->only('name', 'year', 'telp', 'message');
        // dd($data);

        $guest = Guest::create($data);

        return view('confirm');
        // return redirect()->action('SubmitController@return');
    }
}
