@extends('layouts.app')

@section('content')
<div class="container-fluid bg-image">
    <div class="row">
        <div class="midium col-md-4 col-md-push-4 text-center">
            <img class="title-top" src="{{ url('img/title-top.svg') }}" alt="">
            <a class="arrow-link smoothScroll" href="#form">
                <img class="arrow" src="{{ url('img/arrow-down.svg') }}" alt="">
            </a>
        </div>
    </div>
		
    <div id="form" class="row form-row smoothScroll">
    	<div class="col-md-6 col-md-offset-3">
    		
	    	{!! Form::open(['url' => 'guest/submit', 'method'=>'post'])!!}
	    	{!! Form::token() !!}
	          <div class="form-group">
							{!! Form::label('name', 'Nama') !!}
							{!! Form::text('name', null, ['class'=>'form-noon']) !!}
						</div>

						<div class="form-group ">
							{!! Form::label('year', 'Tahun') !!}
							{!! Form::select('year', array('2004'=>'2004', '2005'=>'2005', '2005'=>'2005', '2006'=>'2006', '2007'=>'2007', '2008'=>'2008', '2009'=>'2009', '2010'=>'2010', '2011'=>'2011', '2012'=>'2012', '2013'=>'2013'), null, ['class'=>'form-noon js-selectize']) !!}
							{{-- <span class="form- bamboo">255</span> --}}
						</div>

						<div class="form-group ">
							{!! Form::label('telp', 'Nomor Telepon') !!}
							{!! Form::text('telp', null, ['class'=>'form-noon']) !!}
						</div>

						<div class="form-group ">
							{!! Form::label('message', 'Pesan alumni') !!}
							{!! Form::textarea('message', null, ['class'=>'form-noon', 'style'=>'height: 120px; width: 100%;']) !!}
						</div>

						{!! Form::submit('submit', ['class' => 'submit']) !!}

	      {!! Form::close() !!}
				
				<div class="love text-center">
					Dibuat dengan <i class="fa fa-heart"></i> dari informatika undip
				</div>

    	</div>
    </div>
    <style>
      .indicator{
        background-color: white;
        font-size: 14px;
        color: white;
        position: fixed;
        z-index: 200;
        bottom: 0;
        right: 30px;
        padding: 10px 20px;
      }
    </style>
    @if (Auth::user())
    <div class="indicator">
      <a href="{{ url('home') }}">back to dashbord</a>
    </div>
    @endif

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(function() {
  // This will select everything with the class smoothScroll
  // This should prevent problems with carousel, scrollspy, etc...
  $('.smoothScroll').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });
});

// Change the speed to whatever you want
// Personally i think 1000 is too much
// Try 800 or below, it seems not too much but it will make a difference
</script>
@endsection
