@extends('layouts.app-admin')

@section('admin')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Daftar Buku Tamu</div>
                <div class="panel-body">
                    Cetak Buku Tamu
                <table class="table table-stripped table-bordered table-responsive">
                 <thead>
                    <tr>
                        <td>Nama alumni</td>
                        <td>Tahun angkatan</td>
                        <td>Nomor Telepon</td>
                        <td>Pesan</td>
                        <td>Action</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($guests as $guest)
                    <tr>
                      <td>{{ $guest->name }}</td>
                      <td>{{ $guest->year }}</td>
                      <td>{{ $guest->telp }}</td>
                      <td>{{ $guest->message }}</td>
                      <td>
                      {!! Form::model($guest, ['route' => ['guests.destroy', $guest], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                        <a href="{{ route('guests.edit', $guest->id)}}"><i class="fa fa-pencil-square btn btn-success" style="padding: 1px 5px; display: inline-block; font-size: 10px;"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-danger js-delete-confirm', 'style' => 'padding: 1px 5px; display: inline-block; font-size: 10px;']) !!}
                      {!! Form::close()!!}
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
