@extends('layouts.app')
@section('content')

<div class="container-fluid img-confirm">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<img class="confirm-img" src="{{ url('img/terimakasih.svg') }}" alt="">
			<a href="{{ url('/') }}" class="back">back</a>
		</div>
	</div>
</div>

@endsection