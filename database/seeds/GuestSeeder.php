<?php

use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Guest::create([
        	'name' => 'Rahmad Amiruddin',
        	'year' => '2004',
        	'telp' => '081329209531',
        	'instagram' => 'coldplay',
        	'linkedin' => 'Rahmad Amiruddin',
        	'message' => 'Sukses selalu buat kalian !!!'
        ]);
    }
}
