<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'name' => 'Harum Shidiqi',
        	'email' => 'gathering@gmail.com',
        	'password' => bcrypt('secret'),
        ]);
    }
}
